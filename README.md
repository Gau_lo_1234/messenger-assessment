<img  src="public/images/GAULO LG.png" style="height:80;" />

## @author 
**Gaudencio Solivatore**

## @description

**Coding Challenge**

Design a messaging system using Laravel 8. The system should be able to view, send, receive and delete messages between various users. Restrictions on this you should use tailwind css for any css styling. Use blade templates and partials to their full effect. Also follow DRY and KISS principles.

**Deliverables:**

Git repo including instructions on how to install and run the application in a README file.

**Additional notes:**

The test is intentionally not pre-scaffolded to test that you are comfortable setting up and packing a solution.
Completing the project is important and also is for us to see your own coding style, we are not looking for a carbon copy of an existing solution.



# **Setup the system**
1. `php artisan key:generate`
2. Edit `.env` file for database connection
3. `php artisan migrate`
4.` php artisan db:seed`
4. After all migrations have been populated
5. `php artisan serve`

### **Login samples**

email: test@test.com
password: password

email: test2@test.com
password: password

